<?php

class Billets extends Illuminate\Database\Eloquent\Model {

  protected $table = 'billets';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = ['message','date','titre','id_utilisateur','id_categorie'];

  public static function whereId($id){
    return Billets::where('id', '=', $id)->first();
  }

/*  public static function whereIdCateg($id_categ){
    return Billets::where('id_categorie', '=', $id_categ)->first();
  }*/

  public static function lastTopic(){
    return Billets::orderBy('date', 'DESC')->take(5)->get();
  }

}

?>
