<?php

class Categories extends Illuminate\Database\Eloquent\Model {

  protected $table = 'categories';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = ['label'];

  public static function whereLabel($label){
    return Categories::where('label', '=', $label)->first();
  }

  public static function whereId($id){
    return Categories::where('id', '=', $id)->first();
  }

}

?>
