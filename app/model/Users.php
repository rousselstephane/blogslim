<?php

class Users extends Illuminate\Database\Eloquent\Model {

  protected $table = 'utilisateurs';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = ['pseudo','nom','prenom','mail','mdp','radie'];


  public static function wherePseudo($pseudo){
    return Users::where('pseudo', '=', $pseudo)->first();
  }

  public static function whereId($id){
    return Users::where('id', '=', $id)->first();
  }

}

?>
