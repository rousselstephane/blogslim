<?php

class Commentaires extends Illuminate\database\Eloquent\model {

  protected $table = 'commentaires';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = ['message', 'id_billet', 'id_utilisateur'];

  public static function whereId($id){
    return Commentaires::where('id', '=', $id)->first();
  }

  public static function whereBillet($idBillet){
    return Commentaires::where('id_billet', '=', $idBillet)->get();
  }

}

?>
