<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 04/06/2016
 * Time: 11:16
 */
if (isset($error) && !empty($error_message))
    echo '<font color="red">'.$error_message."</font>";
?>

<br/>
<div id="member_index">
    <h2>Amin tool member</h2>
    Voici la liste des membres présents en base: <br/><br/>
    <form action="" method="post">
        <table>
            <?php
            foreach ($members as $member) {
                if ($member['profil'] === 'membre') {
                    ?>
                    <tr>
                        <td align="right">
                            <?php echo $member['pseudo'] . " &nbsp - &nbsp ";
                            $actif = ($member['radie'] === 0) ? true : false ?>
                            <input type="hidden" id="id_member" name="id_member[]"
                                   value="<?php echo $member['id'] ?>"/>
                        </td>
                        <td>
                            <label for="status">Status :</label>
                            <select id="status" name="status[]">
                                <?php if ($actif) { ?>
                                    <option value="Actif" selected>Actif</option>
                                    <option value="Bann">Bann</option>
                                <?php } else { ?>
                                    <option value="Actif">Actif</option>
                                    <option value="Bann" selected>Bann</option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr>
                <td align="right">
                    <!-- Bouton de validation -->
                    <br/>
                    <input type="submit" value="Valider"/>
                </td>
            </tr>
        </table>
    </form>
</div>



