<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 01/06/2016
 * Time: 20:47
 */
if (isset($error) && !empty($error_message))
    echo "<p class='error'>$error_message</p>";

if(isset($_SESSION['pseudo'])){
    $UserUpdate = Users::wherePseudo($_SESSION['pseudo']);

    if (isset($UserUpdate)) {
?>
        <div align="center">
            <h2>Edition du profil</h2>
            <p>Aller on vous laisse une seconde chance..</p>
            <br/><br/>
            <form action="" method="post">
                <table>
                    <tr>
                        <td align="right">
                            <label for="nom">Pseudo :</label>
                        </td>
                        <td>
                            <input type="text" placeholder="Pseudo" id="pseudoView" name="pseudoView"
                                   disabled="disabled" value="<?php echo $UserUpdate['pseudo'] ?>" />
                            <input type="hidden" placeholder="Pseudo" id="pseudo" name="pseudo"
                                   value="<?php echo $UserUpdate['pseudo'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="nom">Nom :</label>
                        </td>
                        <td>
                            <input type="text" placeholder="Nom" id="new_nom" name="new_nom" maxlength="15"
                                   value="<?php echo $UserUpdate['nom'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="prenom">Prenom :</label>
                        </td>
                        <td>
                            <input type="text" placeholder="Prénom" id="new_prenom" name="new_prenom" maxlength="10"
                                   required="required" value="<?php echo $UserUpdate['prenom'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="mail">Mail :</label>
                        </td>
                        <td>
                            <input type="email" placeholder="Mail" id="new_mail" name="new_mail" maxlength="30"
                                   required="required" value="<?php echo $UserUpdate['mail'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="password">Mot de passe :</label>
                        </td>
                        <td>
                            <input type="password" placeholder="Mot de passe" maxlength="20" id="new_password" name="new_password" required="required" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <label for="pass_check">Confirmation :</label>
                        </td>
                        <td>
                            <input type="password" placeholder="Confirmation du mot de passe" id="new_pass_check" maxlength="20"
                                   required="required" name="new_pass_check"/>
                        </td>
                    </tr>
                    <td></td>
                    <td align="center">
                        <br/>
                        <input type="submit" name="form_update" value="Editer mon profil !"/>
                    </td>
                    </tr>
                </table>
            </form>
            <br/><br/>
            Changement d'avis ?
            <br/>
            On retourne au <a href="<?php echo $app->urlFor("member_account"); ?>">profil</a> !
        </div>
<?php
    }
}
else {
    // redirection
}
?>