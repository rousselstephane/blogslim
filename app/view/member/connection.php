<?php

/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 29/05/2016
 * Time: 18:10
 */

?>

<div align="center">
    <h2>Connexion</h2>
    <br /><br />
    <form action="" method="post">
        <input type="text" name="pseudoCo" placeholder="Votre pseudo" maxlength="10" value="<?php if(isset($pseudo)){ echo $pseudo; }  ?>" />
        <input type="password" name="passwordCo" maxlength="20" placeholder="Mot de passe" />
        <input type="submit" name="formConnect" value="Se Connecter !" />
        <br /><br />
        <?php
        if (isset($error) && !empty($error_message))
            echo '<font color="red">'.$error_message."</font>";
        ?>
    </form>
    <br />
    Pas encore de compte ?
    <br />
    On file vers l' <a href="<?php echo $app->urlFor("member_register"); ?>">inscription</a> !

</div>