<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 01/06/2016
 * Time: 18:34
 */

$userinfo = Users::whereId($id);
?>

    <div align="center">
        <h2>Profil de <?php echo $userinfo['pseudo']; ?> </h2>
        <br/>
        Pseudo = <?php echo $userinfo['pseudo']; ?>
        <br />
        Mail = <?php echo $userinfo['mail']; ?>
        <br /><br/>
        <?php
        if($userinfo['id'] == $_SESSION['id']){
        ?>
            <a href="<?php echo $app->urlFor("member_update"); ?>">Editer mon profil</a>
            <br/>
            <a href="<?php echo $app->urlFor("member_logout"); ?>">Se déconnecter</a>
            <br /><br /><br />
            <a href="<?php echo $app->urlFor("topic_publisher"); ?>">Création</a> d'un nouveau billet !
            <br />
            <?php
        }
        if($userinfo['profil'] == 'admin'){
            ?>
            <br />
            Gestion du blog:
            <a href="<?php echo $app->urlFor("member_index_admin"); ?>">membres</a> &nbsp - &nbsp 
            <a href="<?php echo $app->urlFor("category_index"); ?>">categories</a>
            <br />
            <?php
        }
        if (isset($error) && !empty($error_message))
            echo '<font color="red">'.$error_message."</font>";
        ?>
    </div>