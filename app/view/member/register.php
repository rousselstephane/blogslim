
<?php if (isset($error) && !empty($error_message))
    echo "<p class='error'>$error_message</p>";
?>

<!-- <form action="<?php // echo $app->urlFor('member_register'); ?>" method="post"> -->

<div align="center">
    <h2>Inscription</h2>
    <p>Créez vous un compte ! C'est cool</p>
    <br /><br />
    <form action="" method="post">
        <table>
            <tr>
                <td align="right">
                    <label for="pseudo">Pseudo :</label>
                </td>
                <td>
                    <input type="text" placeholder="Votre pseudo" id="pseudo" name="pseudo" maxlength="10" value="<?php if(isset($pseudo)){ echo $pseudo; }  ?>"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="nom">Nom :</label>
                </td>
                <td>
                    <input type="text" placeholder="Votre nom" id="nom" name="nom" maxlength="15" value="<?php if(isset($nom)){ echo $nom; }  ?>"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="prenom">Prenom :</label>
                </td>
                <td>
                    <input type="text" placeholder="Votre prenom" id="prenom" name="prenom" maxlength="10" value="<?php if(isset($prenom)){ echo $prenom; }  ?>"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="mail">Mail :</label>
                </td>
                <td>
                    <input type="email" placeholder="Votre mail" id="mail" name="mail" maxlength="30" value="<?php if(isset($mail)){ echo $mail; }  ?>"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="password">Mot de passe :</label>
                </td>
                <td>
                    <input type="password" placeholder="Votre mot de passe" maxlength="20" id="password" name="password" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="pass_check">Confirmation :</label>
                </td>
                <td>
                    <input type="password" placeholder="Confirmation MDP" maxlength="20" id="pass_check" name="pass_check" />
                </td>
            </tr>
            <td></td>
            <td align ="center">
                <br />
                <input type="submit" name="forminscription" value="Inscription" />
            </td>
            </tr>
        </table>
    </form>
    <br /><br />
    Déjà un des notres ?
    <br />
    On file vers la <a href="<?php echo $app->urlFor("member_connection"); ?>">connexion</a> !
</div>