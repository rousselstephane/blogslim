<!DOCTYPE html>
<html lang="fr">
    <head >
	<title>Mon super blog</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="scale=1.0" />
	<link rel="stylesheet" href="static/style.css" type="text/css" media="screen" />
    </head>
    <body>

	<section>
	    <div>
		Retour à la <a href="<?php echo $app->urlFor('root');?>">racine</a>
	    </div>
	</section>

	<?php  
	if (isset($flash['info']) && !empty($flash['info']))
	   echo <<<YOP
	<div class="info">
	    {$flash['info']}
	</div>
YOP
	?>

