<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 03/06/2016
 * Time: 00:37
 */

$Categ = Categories::whereId($topic['id_categorie'])->label;
?>

    <div align="center">
        <h2><?php echo $topic['titre']; ?></h2>
        <br/>
    </div>


    <div id="topic_this">
        <h3> <?php echo $Categ; ?> </h3>
        <h4> <?php echo $topic['date']; ?></h4>
        <p>
            <?php echo nl2br($topic['message']."\n\n"); ?>
            <br/><br/>
            <i>Commentaires associés:</i>
        </p>

        <?php
        if($comments = Commentaires::whereBillet($topic['id'])){
            foreach ($comments as $comment) {
                $who = Users::whereId($comment['id_utilisateur']);
                if ($who->radie === 0) {
                    ?>

                    <p><i><?php echo $who['pseudo']; ?></i>
                        le <?php echo $comment['date']; ?></p>
                    <p><?php echo nl2br(($comment['message'])); ?></p>

                    <br/>
                    <?php
                }
            }
        }
        else{
        ?>
            <i>Pas de réaction pour le moment !</i>
        <?php
        }
        ?>
        <br/><br/>
        Souhaitez-vous ajouter un <a href="<?php echo $app->urlFor("comment_pusblisher", ['id' => $topic['id']]); ?>">commentaire</a>
        ou retourner à la liste des <a href="<?php echo $app->urlFor("topic_index"); ?>">derniers billets</a> ?
        <br/><br/>
    </div>