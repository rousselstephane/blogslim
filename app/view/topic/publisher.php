<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 01/06/2016
 * Time: 19:19
 */

if (isset($_SESSION['id'])) {
    $User_sign = Users::whereId($_SESSION['id'])->pseudo;
?>

    <div align="center">
        <h2>Billeterie</h2>
        <br/>
        <p>Park'sa ?</p>
        <br/>
        <!-- Definition zone de formulaire avec cible et type de formulaire -->
        <form action="" method="post">
            <table>
                <tr>
                    <td align="right">
                        <!-- Title -->
                        <label for="titre">Titre :</label>
                    </td>
                    <td>
                        <input type="text" placeholder="Titre du billet" id="titre" name="titre"/>
                        <!-- Catégorie -->
                        <label for="categorie">Catégorie :</label>
                        <select id="categorie" name="categorie">
                            <option value="New">New</option>
                            <option value="Test">Test</option>
                            <option value="Success">Success</option>
                            <option value="General">General</option>
                            <option value="Fun">Fun</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!-- Text -->
                        <label for="message">Contenu :</label>
                    </td>
                    <td>
                        <textarea name="message" rows="12" cols="60"
                                  maxlength="2500"><?php echo "\n\n\n\n@author: ".htmlspecialchars($User_sign); ?></textarea>
                        <!-- Value espacement puis pseudo de l'user pour signature non obligatoire, (?) + n° du billet de l'user (?) -->
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <!-- Bouton de validation -->
                        <input type="submit" value="Valider"/>
                    </td>
                </tr>
            </table>

        </form>
    </div>

<?php
}
else{
    //redirection
}
?>