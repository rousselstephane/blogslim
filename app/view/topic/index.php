<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 03/06/2016
 * Time: 18:44
 */

?>

    <div align="center">
        <h2>Les derniers billets !</h2>
        <br/>
    </div>

<?php
foreach ($topic as $billet){
    $who = Users::whereId($billet['id_utilisateur']);
    if ($who->radie === 0) {
        ?>

        <div id="topic_index">
            <h3> <?php echo $billet['titre']; ?> </h3>
            <h5> <?php echo $billet['date']; ?> </h5>
            <p>
                <?php echo nl2br($billet['message'] . "\n\n"); ?>

                Voir ce billet en <a
                    href="<?php echo $app->urlFor("topic_billet", ['id' => $billet['id']]); ?>">entier</a> !
            </p>
            <br/><br/>
        </div>
        <?php
    }
}
?>