<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 02/06/2016
 * Time: 02:35
 */

if (isset($_SESSION['id'])) {
    $User_sign = Users::whereId($_SESSION['id'])->pseudo;
    ?>

    <div align="center">
        <h2>Commentaire</h2>
        <br/>
        <p>Que souhaitez-vous indiquez à la communautée suite à ce billet</p>
        <br/>
        <!-- Definition zone de formulaire -->
        <form action="" method="post">
            <table>
                <tr>
                    <td>
                        <!-- Text -->
                        <label for="message">Contenu :</label>
                    </td>
                    <td>
                        <textarea name="message" rows="6" cols="35"
                                  maxlength="400"><?php echo "\n\n@author: ".htmlspecialchars($User_sign); ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <!-- Bouton de validation -->
                        <input type="submit" value="Valider"/>
                    </td>
                </tr>
            </table>

        </form>
    </div>

    <?php
}
else{
    //redirection
}
?>