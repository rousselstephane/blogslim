<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 29/05/2016
 * Time: 18:10
 */


if(isset($_POST['forminscription'])){
    $pseudo = htmlspecialchars($_POST['pseudo']);
    $nom = htmlspecialchars($_POST['nom']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $mail = htmlspecialchars($_POST['mail']);
    // Hachage
    $pass_hache = sha1($_POST['password']);
    $pass_hache_check = sha1($_POST['pass_check']);

    if(!empty($_POST['pseudo']) AND !empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['prenom']) AND !empty($_POST['mail']) AND !empty($_POST['password']) AND !empty($_POST['pass_check'])){


        $pseudoLength = strlen($pseudo);
        if($pseudoLength <= 50){
            if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                //  Check mail existant bdd voir note
                //  Check pseudo existant bdd voir note
                    if ($pass_hache == $pass_hache_check) {
                        // INSERT INTO
                        // EXECUTE
                        $champ_err = "Votre compte utilisateur a bien été crée !";
                        // Header redirection vers page membre
                    }
                    else{
                        $champ_err = "Les mots de passe ne correspondent pas !";
                    }
            }
            else{
                $champ_err = "Adresse mail invalide !";
            }
        }
        else{
            $champ_err = "Tranquille la taille du pseudo ?";
        }
    }
    else{
        $champ_err = "Tous les champs doivent être complétés !";
    }
}

?>


<div align="center">
    <h2>Inscription</h2>
    <br /><br />
    <form action="inscription.php" method="post">
        <table>
            <tr>
                <td align="right">
                    <label for="pseudo">Pseudo :</label>
                </td>
                <td>
                    <input type="text" placeholder="Votre pseudo" id="pseudo" name="pseudo" value="<?php if(isset($pseudo)){ echo $pseudo; }  ?>"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="nom">Nom :</label>
                </td>
                <td>
                    <input type="text" placeholder="Votre nom" id="nom" name="nom" value="<?php if(isset($nom)){ echo $nom; }  ?>"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="prenom">Prenom :</label>
                </td>
                <td>
                    <input type="text" placeholder="Votre prenom" id="prenom" name="prenom" value="<?php if(isset($prenom)){ echo $prenom; }  ?>"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="mail">Mail :</label>
                </td>
                <td>
                    <input type="email" placeholder="Votre mail" id="mail" name="mail" value="<?php if(isset($mail)){ echo $mail; }  ?>"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="password">Mot de passe :</label>
                </td>
                <td>
                    <input type="password" placeholder="Votre mot de passe" id="password" name="password" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="pass_check">Confirmation :</label>
                </td>
                <td>
                    <input type="password" placeholder="Confirmation MDP" id="pass_check" name="pass_check" />
                </td>
            </tr>
                <td></td>
            <td align ="center">
                <br />
                <input type="submit" name="forminscription" value="Inscription" />
            </td>
            </tr>
        </table>
    </form>

    <?php
    if(isset($champ_err)){
        echo '<font color="red">'.$champ_err."</font>";
    }
    ?>
</div>


