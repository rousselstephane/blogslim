<br/>
<div id="category_edit">
    <form method="post" action="<?php echo $app->urlFor('category_update'); ?>">
        <input type="hidden" name="id_category" value="<?php echo (isset($id_category) ? $id_category : ''); ?>"/>

        <label for="category_name">Nom de la catégorie</label>
        <input type="text" id="category_name" name="category_name" required="required" value="<?php echo (isset($category_name) ? $category_name : ''); ?>"/>
        <?php
        if (isset($error) && !empty($error_message))
            echo '<font color="red">'.$error_message."</font>";
        ?>
        <br/><br/>
        <input type="submit" value="Aller ajoute!!"/>
        <br/><br/>
        Retour à la lites des <a href="<?php echo $app->urlFor("category_index"); ?>">catégories</a> !
    </form>
</div>
