<br/>
<div id="category_index">
    Voici la liste des catégories présentes en base: <br/>
    <ul>
        <?php
            foreach ($categories as $category) {
                echo "<li><a href='{$app->urlFor('category_edit', ['id' => $category->id])}'>$category->label</a></li>";
            }
        ?>
    </ul>

    Vous pouvez aussi en créer <a href="<?php echo $app->urlFor('category_create'); ?>">ici</a> !
</div>
