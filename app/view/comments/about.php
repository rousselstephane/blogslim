<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 03/06/2016
 * Time: 18:50
 */

?>
    <div align="center">
        <h2>Les réactions !</h2>
        <br/>
    </div>

<?php
foreach ($comments as $comment){
    $who = Users::whereId($comment['id_utilisateur'])->pseudo;
    ?>

    <div id="comments_about">
        <p><strong><?php echo $who; ?></strong>
            le <?php echo $comment['date']; ?></p>
        <p><?php echo nl2br(($comment['message'])); ?></p>
    </div>

    <?php
}
?>

Retour à la liste des <a href="<?php echo $app->urlFor("topic_index"); ?>">derniers billets</a> !
