<?php
/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 02/06/2016
 * Time: 02:35
 */


if (isset($error) && !empty($error_message))
    echo '<font color="red">'.$error_message."</font>";
?>

    <div align="center">
        <h2>Commentaire</h2>
        <br/>
        <p>Que souhaitez-vous indiquez à la communautée suite à ce billet</p>
        <br/>
        <!-- Definition zone de formulaire -->
        <form action="" method="post">
            <table>
                <tr>
                    <td>
                        <!-- Text -->
                        <label for="name">Contenu de votre commentaire:</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea id="message" name="message" rows="6" cols="35"
                                  maxlength="400"><?php echo "" ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <!-- Bouton de validation -->
                        <input type="submit" value="Valider"/>
                    </td>
                </tr>
            </table>

        </form>
    </div>