<?php

use Slim\Slim;

class Controller {

    // Initialization impossible here: Slim does not yet exist!
    /** @var Slim */
    protected static $app;

    public function __construct(){
	if (empty(Controller::$app))
	    Controller::$app = Slim::getInstance();
    }

    public function renderPage($pagePath, $data = array()) {
        self::$app->render('header.php', ['app' => self::$app]);
        self::$app->render($pagePath, $data);
        self::$app->render('footer.php');
    }
}

?>
