<?php

/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 30/05/16
 * Time: 19:22
 */

class CommentaireController extends Controller
{
    /**
     * commentaire sur un billet existant (membres) : saisie d’un texte, ajout de la date automatique
     */
    public function create($id)
    {
        $error = false;
        $error_message = '';
        $comment = false;

        if (isset($_SESSION['id']) AND isset($_SESSION['pseudo'])) {
            if (self::$app->request->isPost()) {
                $message = filter_var(self::$app->request->post('message'), FILTER_SANITIZE_STRING);
                $id_billet = $id;
                $topic = Billets::whereId($id_billet);
                $id_utilisateur = $_SESSION['id'];

                if (empty($message)) {
                    $error = true;
                    $error_message = "Vous n'avez pas saisie de message pour votre réponse au billet !";
                }
                if (empty($id_billet)) {
                    $error = true;
                    $error_message = "Pour poster un commentaire il faudrait qu'il réponde à un topic !";
                }
                if (empty($id_utilisateur)) {
                    $error = true;
                    $error_message = "Connecte toi petit malin !";
                }

                if (!$error) {
                    $comment = new Commentaires(['message' => $message, 'id_billet' => $id_billet, 'id_utilisateur' => $id_utilisateur]);
                    if ($comment->save()) {
                        $comment = true;
                        $this->renderPage('topic/billet.php', ['app' => self::$app, 'topic' => $topic]);
                    }
                    else {
                        $error = true;
                        $error_message = '';
                        $this->erreur();
                    }
                }
            }
        }
        else {
            self::$app->redirectTo('member_connection', ['error' => $error, 'error_message' => $error_message]);
        }

        if(!$comment)
            $this->renderPage('comments/publisher.php', ['error' => $error, 'error_message' => $error_message]);
    }

    /**
     * affichage des commentaires associés a un billet
     */
    public function views($id_topic)
    {
        $this->renderPage('comments/about.php', ['app' => self::$app, 'comments' => Commentaires::whereBillet($id_topic)]);
    }

}
