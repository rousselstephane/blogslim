<?php

/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 30/05/16
 * Time: 19:21
 */

class BilletController extends Controller
{
    /**
     * saisie d’un billet (membres) : titre, message et choix d’une catégorie parmi celles existantes, ajout de la date
     * automatique
     */
    public function create()
    {
        $error = false;
        $error_message = '';

        if (self::$app->request->isPost()) {
            $titre = filter_var(self::$app->request->post('titre'), FILTER_SANITIZE_STRING);
            $message = filter_var(self::$app->request->post('message'), FILTER_SANITIZE_STRING);
            $categ_label = self::$app->request->post('categorie');
            $categ = Categories::whereLabel($categ_label); // On récupère l'objet catégorie correspondant au choix de l'user

            if (empty($titre)) {
                $error = true;
                $error_message = "Vous n'avez pas saisie de titre pour votre billet";
            }
            if (empty($message)) {
                $error = true;
                $error_message = "Votre billet est vide !";
            }

            if (!$error) {
                $billet = new Billets(['message' => $message, 'titre' => $titre, 'id_utilisateur' => $_SESSION['id'], 'id_categorie' => $categ['id']]);

            if ($billet->save())
                    self::$app->redirectTo('member_account');
                else {
                    $error = true;
                    $error_message = '';
                    $this->erreur();
                }
            }
        }
        $this->renderPage('topic/publisher.php', ['error' => $error, 'error_message' => $error_message]);
    }


    /**
     * affichage d’un billet complet (public) : titre, date, message complet, catégorie, commentaires associés
     */
    public function view($id_topic)
    {
        $topic = Billets::whereId($id_topic);
        $this->renderPage('topic/billet.php', ['app' => self::$app, 'topic' => $topic]);
    }

    /**
     * affichage des derniers billets (public) : titre, date, message complet, catégorie, lien commentaires associés
     */
    public function viewLast()
    {
        $this->renderPage('topic/index.php', ['app' => self::$app, 'topic' => Billets::lastTopic()]);
    }


    /**
     * affichage des commentaires associés
     */
    public function viewComm($id_topic)
    {
        $this->renderPage('topic/commentaires.php', ['app' => self::$app, 'topic' => $id_topic]);
    }

    /**
     * affichage de la liste des billets (public), par pages de 20 billets, avec pour chacun d’eux : titre, date, catégorie,
     * début du texte (les 30 premiers caractères), par ordre anti-chronologique
     */
    public function index()
    {
        $order = Billets::all();
        //   ---@
    }

    /**
     * statistiques de répartition des billets par catégorie (public) par un camembert 3D interactif et affichage des
     * billets d’une catégorie choisie
     */
    public function divisionStats()
    {
        //   ---@
    }

    /**
     * supprime le billet spécifié (administrateur, modérateur, membre possédant le billet)
     */
    public function destroy($id)
    {
       Billets::destroy($id);
    }
}
