<?php

class MemberController extends Controller
{
    /**
     * inscription d’un nouveau membre (public) (pseudo, nom, prénom, mail, mot de passe)
     */
    public function register()
    {
        $error = false;
        $error_message = '';
        if($this->cooking()){
            if (isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
                $this->renderPage('member/account.php', ['id' => $_SESSION['id']]);
            }
        }
        else {
            if (self::$app->request->isPost()) {
                $pseudo = filter_var(self::$app->request->post('pseudo'), FILTER_SANITIZE_STRING); //Supprimer les balises
                $nom = filter_var(self::$app->request->post('nom'), FILTER_SANITIZE_STRING);
                $prenom = filter_var(self::$app->request->post('prenom'), FILTER_SANITIZE_STRING);
                $mail = filter_var(self::$app->request->post('mail'), FILTER_SANITIZE_STRING);
                $mdp = filter_var(self::$app->request->post('password'), FILTER_SANITIZE_STRING);
                $mdpConfirm = filter_var(self::$app->request->post('pass_check'), FILTER_SANITIZE_STRING);

                if ($mdp != $mdpConfirm) {
                    $error = true;
                    $error_message = "Les mots de passes sont différents...";
                }

                if (empty($pseudo) || empty($nom) || empty($prenom) || empty($mail) || empty($mdp)) {
                    $error = true;
                    $error_message = "Tous les champs sont obligatoires";
                }

                if (!$error) {
                    $grain = "B;g60dT1!2";
                    $codage = sha1(sha1($grain.$mdp).sha1($grain.$grain));
                    $user = new Users(['pseudo' => $pseudo, 'nom' => $nom, 'prenom' => $prenom, 'mail' => $mail, 'mdp' => $codage, 'radie' => 0]);

                    if ($user->save()) {
                        self::$app->redirectTo('member_connection');
                    }
                    else {
                        $error = true;
                        $error_message = '';
                        $this->erreur();
                    }
                }
            }
            if($error || !isset($_SESSION['id'])) {
                $this->renderPage('member/register.php', ['error' => $error, 'error_message' => $error_message]);
            }
        }
    }


    public function registerSuccess()
    {
        $this->renderPage('member/registerSuccess.php', ['app' => self::$app, 'pseudo' => self::$app->request->get('pseudo')]);
    }


    public function account()
    {
        if (isset($_SESSION['id']) AND isset($_SESSION['pseudo'])) {
            $this->renderPage('member/account.php', ['id' => $_SESSION['id']]);
        }
        else {
/*            $error = true;
            $error_message = "Vous devez vous connecter pour accéder à votre profil.";*/
            self::$app->redirectTo('member_connection'); // ['error' => $error, 'error_message' => $error_message]
        }
    }


    /**
     * connexion d’un membre (membres)
     */
    public function connect()
    {
        $error = false;
        $error_message = '';

        if($this->cooking()){
            if (isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
                $this->renderPage('member/account.php', ['id' => $_SESSION['id']]);
            }
        }
        else {
            if (self::$app->request->isPost()) {
                $pseudo = filter_var(self::$app->request->post('pseudoCo'), FILTER_SANITIZE_STRING);
                $mdp = filter_var(self::$app->request->post('passwordCo'), FILTER_SANITIZE_STRING);
                if (!empty($pseudo && !empty($mdp))) {
                    $membreCo = Users::wherePseudo($pseudo);
                    if (!empty($membreCo)) {
                        $seed = "B;g60dT1!2";
                        $counter = sha1(sha1($seed.$mdp).sha1($seed.$seed));
                        $check = $membreCo->mdp;
                        if ($check === $counter && $membreCo->radie === 0) {
                            $_SESSION['id'] = $membreCo->id;
                            $_SESSION['pseudo'] = $membreCo->pseudo;
                            $_SESSION['mail'] = $membreCo->mail;
                            $value = $membreCo->id.'---'.hash('sha512', $membreCo->pseudo.'---'.$membreCo->mdp);
                            setcookie('auth', $value, time() + (7 * 24 * 3600) , null, null, false, true);
                            $this->renderPage('member/account.php', ['id' => $_SESSION['id']]);
                        } else {
                            if($membreCo->radie === 1){
                                $error = true;
                                $error_message = "Utilisateur radié !";
                            }
                            else {
                                $error = true;
                                $error_message = "Login ou password invalide !";
                            }
                        }
                    }
                    else {
                        $error = true;
                        $error_message = "Utilisateur non enregistré !";
                    }
                }
                else {
                    $error = true;
                    $error_message = "Veuillez renseigner des identifiants";
                }
            }
            if($error || !isset($_SESSION['id'])) {
                $this->renderPage('member/connection.php', [
                        'error' => $error,
                        'error_message' => $error_message]
                );
            }
        }
    }


    /**
     * déconnexion d’un membre (membres)
     */
    public function logout()
    {
        $_SESSION = array();
        session_destroy();
        self::$app->redirectTo('member_connection');
    }


    /**
     * modification des informations d’un compte utilisateur (membres) : nom, prénom, mail et mot de passe unique-
     * ment (pas le pseudo donc)
     */
    public function update()
    {
        $error = false;
        $error_message = '';

        if (self::$app->request->isPost()) {
            $pseudo = filter_var(self::$app->request->post('pseudo'), FILTER_SANITIZE_STRING); //Supprimer les balises
            $nom = filter_var(self::$app->request->post('new_nom'), FILTER_SANITIZE_STRING);
            $prenom = filter_var(self::$app->request->post('new_prenom'), FILTER_SANITIZE_STRING);
            $mail = filter_var(self::$app->request->post('new_mail'), FILTER_SANITIZE_STRING);
            $mdp = filter_var(self::$app->request->post('new_password'), FILTER_SANITIZE_STRING);
            $mdpConfirm = filter_var(self::$app->request->post('new_pass_check'), FILTER_SANITIZE_STRING);

            if (empty($nom) || empty($prenom) || empty($mail) || empty($mdp) || empty($mdpConfirm)) {
                $error = true;
                $error_message = "Tous les champs sont obligatoires";
            }

            if ($mdp != $mdpConfirm) {
                $error = true;
                $error_message = "Les mots de passes sont différents...";
            }

            if (!$error) {
                $grain = "B;g60dT1!2";
                $codage = sha1(sha1($grain.$mdp).sha1($grain.$grain));
                $userUp = Users::wherePseudo($pseudo);
                $userUp->nom = $nom;
                $userUp->prenom = $prenom;
                $userUp->mail = $mail;
                $userUp->mdp = $codage;

                if ($userUp->save()) {
                    $_SESSION['mail'] = $userUp->mail;
                    $value = $userUp->id.'---'.hash('sha512', $userUp->pseudo.'---'.$userUp->mdp);
                    setcookie('auth', $value, time() + (7 * 24 * 3600) , null, null, false, true);
                    self::$app->redirectTo('member_account');
                }
                else {
                    $error = true;
                    $error_message = '';
                    $this->erreur();
                }
            }
        }

        $this->renderPage('member/update.php', array(
                        'error'             => $error,
                        'error_message'     => $error_message
                        ));
    }


    /**
     * affichage de la liste des membres (administrateur), avec possibilité de radier un ou plusieurs membres à partir
     * de cette liste. En cas de radiation, les membres correspondants ne peuvent plus se connecter, leurs billets
     * n’apparaissent plus et leurs commentaires non plus
     */
    public function index()
    {
        $error = false;
        $error_message = '';

        if (isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
            $user = Users::whereId($_SESSION['id']);
            if($user->profil === 'admin'){
                if (self::$app->request->isPost()) {
                    $id_member[] = self::$app->request->post('id_member');
                    $status[] = self::$app->request->post('status');
                    $result = array();
                    for($i=0 ; $i < sizeof($id_member[0]); $i++){
                        $result[$i][0] = $id_member[0][$i];
                        if($status[0][$i] === 'Actif'){
                            $result[$i][1] = 0;
                        }
                        else{
                            $result[$i][1] = 1;
                        }
                    }
                    for($i=0 ; $i < sizeof($result); $i++){
                        $user = Users::whereId($result[$i][0]);
                        if($user->profil === 'membre'){
                            $user->radie = $result[$i][1];
                            if($user->save()) {
                                $result[$i][2] = 1;
                            }
                            else {
                                $error = true;
                                $error_message = 'Erreur sur le membre '.$result[$i][0];
                            }
                        }
                        else{
                            $error = true;
                            $error_message = "L'utilisateur ".$result[$i][0]." n'est pas un simple membre...";
                        }
                    }
                    if(!$error)
                        $this->renderPage('member/index.php', ['error' => $error, 'error_message' => 'Changements enregistrés', 'members' => Users::all()]);

                }   //  Pas de formulaire
                else
                    $this->renderPage('member/index.php', ['app' => self::$app, 'members' => Users::all()]);
            }   //  Accès par non Admin
            else {
                $error = true;
                $error_message = "Vous n'êtes pas autorisé à accéder ette page";
            }
        }   //  Accès par non connecté
        else {
            $error = true;
            $error_message = 'Veuillez vous connecter pour accéder à cette page';
        }
        if($error) {
            if(isset($_SESSION['id']))
                $this->renderPage('member/account.php', ['error' => $error, 'error_message' => $error_message, 'id' => $_SESSION['id']]);
            else
                self::$app->redirectTo('member_connection');
        }
    }

    /**
     *  Contrôle la présence du cookie d'authentification
     */
    public function cooking()
    {
        $hap = false;
        if (isset($_COOKIE['auth'])) {
            $auth = explode('---', $_COOKIE['auth']);
            if (count($auth) === 2) {
                $user = Users::whereId($auth[0]);

                if ($user && $auth[1] === hash('sha512',  $user->pseudo.'---'.$user->mdp)) {
                    $_SESSION['id'] = $user->id;
                    $_SESSION['pseudo'] = $user->pseudo;
                    $hap = true;
                }
            }
        }
        else{
            $hap = false;
        }
        return $hap;
    }

}
