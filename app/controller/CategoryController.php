<?php

/**
 * Created by PhpStorm.
 * User: Moussaka
 * Date: 30/05/16
 * Time: 19:25
 */

class CategoryController extends Controller
{
    /**
     * ajout d’une catégorie (administrateur), avec vérification de la non-existance préalable de cette catégorie
     */
    public function create()
    {
        $this->renderPage('category/edit.php', ['app' => self::$app]);
    }

    public function index()
    {
        $this->renderPage('category/index.php', ['app' => self::$app, 'categories' => Categories::all()]);
    }

    public function edit($id_category)
    {
        if ($id_category !== null && $id_category > 0) {
            /** @var Categories $category */
            $category = Categories::find($id_category);

            if ($category !== null) {
                $this->renderPage('category/edit.php', ['id_category' => $category->id, 'category_name' => $category->label]);
            } else {
                $this->renderPage('category/edit.php');
            }
        } else {
            $this->renderPage('category/edit.php');
        }
    }

    public function insertOrUpdate()
    {
        $error = false;
        $error_message = '';

        $id_category = self::$app->request->post('id_category');
        $label = self::$app->request->post('category_name');
        $check = Categories::whereLabel($label);
        if(empty($check)) {
            $category = !empty($id_category) ? Categories::find($id_category) : new Categories();
            $category->label = $label;
            if($category->save())
                self::$app->redirectTo('category_index');
        }
        else{
            $error = true;
            $error_message = 'Cette catégorie existe déjà !';
        }
        if($error) {
            $this->renderPage('category/edit.php', [
                    'error' => $error,
                    'error_message' => $error_message]
            );
        }
    }

    
}
